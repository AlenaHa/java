/**
 * @author Elena Hardon
 * @date 3/15/17.
 */
public class DynamicKnapsack {


    // A utility function that returns maximum of two integers
    static int max(int a, int b) {
        return (a > b) ? a : b;
    }

    // Returns the maximum value that can be put in a knapsack of capacity W
    static int knapSack(int maximumWeigth, int weightVector[], int valueVector[], int numberOfItems) {
        int itemsIterator, weigthIterator;
        int K[][] = new int[numberOfItems + 1][maximumWeigth + 1];

        for (itemsIterator = 0; itemsIterator <= numberOfItems; itemsIterator++) {
            for (weigthIterator = 0; weigthIterator <= maximumWeigth; weigthIterator++) {
                if (itemsIterator == 0 || weigthIterator == 0)
                    K[itemsIterator][weigthIterator] = 0;
                else if (weightVector[itemsIterator - 1] <= weigthIterator)
                    K[itemsIterator][weigthIterator] = max(valueVector[itemsIterator - 1] + K[itemsIterator - 1][weigthIterator - weightVector[itemsIterator - 1]], K[itemsIterator - 1][weigthIterator]);
                else
                    K[itemsIterator][weigthIterator] = K[itemsIterator - 1][weigthIterator];
            }
        }

        return K[numberOfItems][maximumWeigth];
    }

    public static void main(String args[]) {
        int valuesVector[] = new int[]{60, 100, 120};
        int weightVector[] = new int[]{10, 20, 30};
        int totalWeight = 50;
        int numberOfItems = valuesVector.length;
        System.out.println(knapSack(totalWeight, weightVector, valuesVector, numberOfItems));
    }
}
