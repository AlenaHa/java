/**
 * @author Elena Hardon
 * @date 3/6/17.
 */
public abstract class AbstractItem implements Comparable<AbstractItem>, Item {
    private String itemName;
    private int itemWeight;
    private double itemValue;
    private double itemProfit;


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemValue() {
        return itemValue;
    }

    public void setItemValue(double itemValue) {
        this.itemValue = itemValue;
    }

    public int getItemWeight() {
        return itemWeight;
    }

    public void setItemWeight(int itemWeight) {
        this.itemWeight = itemWeight;
    }

    public void setItemProfit(double itemProfit) {
        this.itemProfit = itemProfit;
    }


    public double getProfitFactor() {
        itemProfit = getProfitFactor(this.itemValue, this.itemWeight);
        return itemProfit;
    }

    @Override
    public int compareTo(AbstractItem o) {
        if (this.getProfitFactor() == o.getProfitFactor()) {
            return 0;
        } else if (this.getProfitFactor() < o.getProfitFactor()) {
            return 1;
        } else {
            return -1;
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("This item has: ");
        sb.append("itemName = '").append(itemName).append('\'');
        sb.append(", itemValue = ").append(itemValue);
        sb.append(", itemWeight = ").append(itemWeight);
        sb.append(", itemProfit = ").append(getProfitFactor());
        sb.append('}');
        return sb.toString();
    }
}
