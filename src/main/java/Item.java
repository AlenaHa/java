/**
 * @author Elena Hardon
 * @date 3/6/17.
 */
public interface Item {
    default double getProfitFactor(double itemValue, int itemWeight) {
        double itemProfit = itemValue / (double) itemWeight;
        return itemProfit;
    }
}
