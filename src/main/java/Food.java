/**
 * @author Elena Hardon
 * @date 3/6/17.
 */
public class Food extends AbstractItem implements Item {

    public Food(String itemName, int itemWeight, double itemValue) {
        this.setItemName(itemName);
        this.setItemValue(itemValue);
        this.setItemWeight(itemWeight);
    }

}
