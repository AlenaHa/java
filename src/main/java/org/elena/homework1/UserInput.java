package org.elena.homework1;

import java.util.Scanner;

/**
 * @author Elena Hardon
 * @date 2/22/17.
 */
public class UserInput {

    /**
     * This method sets the arguments for the latin and greeek arrays from user input.
     */
    public static void getUserInput() {

        Scanner scanner = null;
        int size;
        String matrixType;
        try {
            System.out.println("Insert dimension: ");
            scanner = new Scanner(System.in);
            size = scanner.nextInt();
            matrixType = scanner.next();
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }

        if (matrixType.equals("latin")) {
            AlphabetGenerator latin = new AlphabetGenerator(size, Language.latin);
            latin.printArray(Language.latin);
        } else if (matrixType.equals("greek")) {
            AlphabetGenerator greek = new AlphabetGenerator(size, Language.greek);
            greek.printArray(Language.greek);
        }
    }

    public static void main(String[] args) {
        MatrixGenerator matrixGenerator = MatrixGenerator.getInstance();
        Integer size = Integer.parseInt(args[0]);
        if (args[1].equals("latin")) {
            matrixGenerator.generateMatrix(size, Language.latin);
        } else if (args[1].equals("greek")) {
            matrixGenerator.generateMatrix(size, Language.greek);
        }
    }
}
