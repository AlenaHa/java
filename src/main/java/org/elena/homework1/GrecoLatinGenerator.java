package org.elena.homework1;

import java.util.List;
import java.util.Scanner;

/**
 * @author Elena Hardon
 * @date 2/3/17.
 */
public class GrecoLatinGenerator {

    // The size of the matrix.
    private static Integer grecoLatinMatrixSize;
    // The list of latin characters.
    private static List<Character> latinInputArray;
    // The list of greek characters.
    private static List<Character> greekInputArray;
    // The matrix of grecoLatin charPair objects.
    private static CharPairMatrix grecoLainCharPairMatrix;


    /**
     * This constructor uses the private method setCommandLineArguments() in order to initialize the members of the class.
     */
    public GrecoLatinGenerator() {
        setCommandLineArguments();
    }

    /**
     * @param matrixSize This parameter sets the length of the matrix and the number of latin & greek letters to be generated.
     */
    public GrecoLatinGenerator(Integer matrixSize) {
        grecoLatinMatrixSize = matrixSize;
        AlphabetGenerator greekAlphabetObj = new AlphabetGenerator(matrixSize, Language.greek);
        greekInputArray = greekAlphabetObj.alphabetArray;
        AlphabetGenerator latinAlphabetObj = new AlphabetGenerator(matrixSize, Language.latin);
        latinInputArray = latinAlphabetObj.alphabetArray;
    }

    /**
     * This method itializes the members of the GrecoLatinGenerator class using user input.
     */
    private void setCommandLineArguments() {
//        String latinInputString;
//        String greekInputString;
        Scanner lineScanner = new Scanner(System.in);

        // Setting the number of elements from the matrix.
        System.out.println("Set n:number of elements");
        grecoLatinMatrixSize = lineScanner.nextInt();

        AlphabetGenerator greekAlphabetObj = new AlphabetGenerator(grecoLatinMatrixSize, Language.greek);
        greekInputArray = greekAlphabetObj.alphabetArray;
        AlphabetGenerator latinAlphabetObj = new AlphabetGenerator(grecoLatinMatrixSize, Language.latin);
        latinInputArray = latinAlphabetObj.alphabetArray;
    }


    /**
     * This method returns the grecoLatin matrix that on each row and column contains different grecoLatin charPair objects.
     *
     * @return The charPair matrix.
     */
    public static CharPairMatrix generateGrecoLatinMatrix(int size) {
        CharPairMatrix grecoLatinMatrix;
        // Initialising the matrix with the latin characters only of the CharPair objects.
        grecoLatinMatrix = MatrixGenerator.getInstance().generateMatrix(size, Language.latin);

        // Adding the greek characters(into the CharPair objects) on the main diagonal of the matrix.
        for (int i = 0; i < size; i++) {
            CharPair charPair = grecoLatinMatrix.getElement(i, i);
            charPair.setGreek(greekInputArray.get(0));
            grecoLatinMatrix.setElement(i, i, charPair);
        }

        // Adding the greek characters(into the CharPair objects) above the main diagonal of the matrix.
        for (int i = 0; i < grecoLatinMatrixSize; i++) {
            int k = 1;
            for (int j = i + 1; j < grecoLatinMatrixSize; j++) {
                CharPair charPair = grecoLatinMatrix.getElement(i, j);
                charPair.setGreek(greekInputArray.get(k));
                grecoLatinMatrix.setElement(i, j, charPair);
                k++;
            }
        }
        // Adding the greek characters(into the CharPair objects) below the main diagonal of the matrix.
        for (int i = 0; i < grecoLatinMatrixSize; i++) {
            int k = grecoLatinMatrixSize - i;
            for (int j = 0; j < i; j++) {
                CharPair charPair = grecoLatinMatrix.getElement(i, j);
                charPair.setGreek(greekInputArray.get(k));
                k++;
            }
        }

        grecoLainCharPairMatrix = grecoLatinMatrix;
        return grecoLatinMatrix;
    }

    /**
     * This method prints the GrecoLatin matrix on the screen.
     */
    public static void printGrecoLatinMatrix() {
        for (int i = 0; i < grecoLatinMatrixSize; i++) {
            for (int j = 0; j < grecoLatinMatrixSize; j++) {
                System.out.print(grecoLainCharPairMatrix.getElement(i, j).getLatin());
                System.out.print(grecoLainCharPairMatrix.getElement(i, j).getGreek());
                System.out.print(' ');
            }
            System.out.println();
        }
    }
}
