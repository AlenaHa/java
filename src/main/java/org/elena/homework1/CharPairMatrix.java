package org.elena.homework1;


import java.util.ArrayList;
import java.util.List;

/**
 * @author Elena Hardon
 * @date 2/6/17.
 */
public class CharPairMatrix {
    private List<List<CharPair>> matrix;
    private int matrixSize;

    /**
     * This method creates a matrix of charPair objects.
     *
     * @param size This parameter gives the size of the matrix.
     */
    CharPairMatrix(int size) {

        matrixSize = size;
        matrix = new ArrayList<>(size);
        // Allocate memory for each row.
        for (int i = 0; i < size; i++) {
            matrix.add(i, new ArrayList<>());
            // Allocate memory for each object in each row.
            for (int j = 0; j < size; j++) {
                matrix.get(i).add(j, new CharPair());
            }
        }
    }


    /**
     * This method returns the matrix member of the class created by the constructor.
     *
     * @return Returns the matrix of charPair objects.
     */
    public List<List<CharPair>> getMatrix() {
        return matrix;
    }

    /**
     * This method changes the reference of the previous matrix to the new one.
     *
     * @param matrix The new matrix that is referenced.
     */
    public void setMatrix(List<List<CharPair>> matrix) {
        this.matrix = matrix;
    }

    /**
     * This method sets a charPair element in the matrix.
     *
     * @param rowIndex    The index of the matrix row.
     * @param columnIndex The index of the column row.
     * @param charPair    The charPair object that is about to be set in to the matrix.
     */
    void setElement(int rowIndex, int columnIndex, CharPair charPair) {
        this.matrix.get(rowIndex).set(columnIndex, charPair);
    }

    /**
     * This method returns a charPair element of the matrix.
     *
     * @param rowIndex    The index of the matrix row.
     * @param columnIndex The index of the matrix column.
     * @return The CharPair set of grecoLatin characters.
     */
    public CharPair getElement(int rowIndex, int columnIndex) {
        return this.matrix.get(rowIndex).get(columnIndex);
    }


    /**
     * This method prints a matrix.
     *
     * @param matrixType The type of matrix to be printed (greek or latin).
     */
    public void printMatrix(Language matrixType) {
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                if (matrixType.equals(Language.greek)) {
                    System.out.print(matrix.get(i).get(j).getGreek());
                } else {
                    System.out.print(matrix.get(i).get(j).getLatin());
                }

                System.out.print(' ');
            }
            System.out.println();
        }

    }

}
