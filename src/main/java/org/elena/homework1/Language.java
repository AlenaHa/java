package org.elena.homework1;

/**
 * @author Elena Hardon
 * @date 2/27/17.
 */
public enum Language {
    greek, latin
}
