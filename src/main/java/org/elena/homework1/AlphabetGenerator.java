package org.elena.homework1;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Elena Hardon
 * @date 2/26/17.
 */
public class AlphabetGenerator {

    public List<Character> alphabetArray = new ArrayList<>();
    public List<String> allAlphabetComboStrings;


    /**
     * This represents the constructor that initializes the alphabetArray depending on the language of the alphabet.
     *
     * @param alphabetType The type of the alphabet to be generated.
     */
    public AlphabetGenerator(Language alphabetType) {
        this.generateAlphabet(26, alphabetType);
    }

    /**
     * This represents the constructor that initializes the alphabetArray with a specific number of characters of a specified language.
     *
     * @param numberOfCharacters The number of characters to be generated.
     * @param alphabetType       The type of the alphabet to be generated.
     */
    public AlphabetGenerator(int numberOfCharacters, Language alphabetType) {
        this.generateAlphabet(numberOfCharacters, alphabetType);
    }


    /**
     * This function is used in the constructors in order to generate a specific alphabet.
     *
     * @param numberOfCharacters The number of characters to be generated.
     * @param alphabetType       The type of the alphabet to be generated.
     */
    private void generateAlphabet(int numberOfCharacters, Language alphabetType) {

        if (alphabetType.equals(Language.greek)) {
            for (int i = 0; i <= numberOfCharacters; i++) {
                int greekLetter = '\u03B1' + i;
                alphabetArray.add(((char) greekLetter));
            }
        } else if (alphabetType.equals(Language.latin)) {
            for (Character character = 'A'; character <= 'Z' && numberOfCharacters != 0; character++) {
                alphabetArray.add(character);
                numberOfCharacters--;
            }
        } else {
            System.out.println("Wrong alphabet type");
        }

        allAlphabetComboStrings = new ArrayList<String>(countAllPossibleCombinations());
        String alphabetString = "";
        for (int i = 0; i < alphabetArray.size(); i++) {
            alphabetString = alphabetString + alphabetArray.get(i);
        }
        combineAlphabetString(" ", alphabetString);
    }

    /**
     * This method prints the alphabetArray member.
     *
     * @param arrayType The type of alphabet that is about to be printed.
     */
    public void printArray(Language arrayType) {
        if (arrayType.equals("latin")) {
            for (Character character : alphabetArray) {
                System.out.println(character);
            }
        } else if (arrayType.equals("greek")) {
            for (Character greekCharacter : alphabetArray) {
                System.out.println(greekCharacter);
            }
        } else {
            System.out.println("Wrong alphabet arrayType");
        }
    }

    /**
     * This method returns the number of possible combination strings for a given length.
     *
     * @return
     */
    private int countAllPossibleCombinations() {
        int sizeOfAlphabet = 1;
        for (int i = alphabetArray.size(); i > 0; i--) {
            sizeOfAlphabet = sizeOfAlphabet * i;
        }
        return sizeOfAlphabet;
    }

    /**
     * This method is a recursive one and it populates the array of all Strings that represent combinations for a given alphabet.
     *
     * @param prefix
     * @param alphabetString
     */
    private void combineAlphabetString(String prefix, String alphabetString) {
        int N = alphabetString.length();

        if (N == 0)
            allAlphabetComboStrings.add(prefix);

        for (int i = 0; i < N; i++)
            combineAlphabetString(prefix + alphabetString.charAt(i), alphabetString.substring(0, i) + alphabetString.substring(i + 1, N));
    }

}
