package org.elena.homework1;

/**
 * @author Elena Hardon
 * @date 2/6/17.
 */

/**
 * This class provides the component for the future greco-latin matrix that consists in a greek and a latin character.
 */
public class CharPair {
    Character greek;
    Character latin;

    public CharPair() {
    }

    /**
     * Constructor for the CharPair class.
     *
     * @param greek Provides the greek character of the pair.
     * @param latin Provides the latin character of the pair.
     */
    public CharPair(Character greek, Character latin) {
        this.greek = greek;
        this.latin = latin;
    }

    /**
     * This method returns the greek character of the charPair set.
     *
     * @return The greek character of the pair.
     */
    public Character getGreek() {
        return greek;
    }

    /**
     * This method sets the greek character of the pair.
     *
     * @param greek The greek character that is about to be set in the pair.
     */
    public void setGreek(Character greek) {
        this.greek = greek;
    }

    /**
     * This method returns the latin character of the charPair.
     *
     * @return The latin character or the pair.
     */
    public Character getLatin() {
        return latin;
    }

    /**
     * This method sets the latin character of the pair.
     *
     * @param latin The latin character that is about to be set in the pair.
     */
    public void setLatin(Character latin) {
        this.latin = latin;
    }

    /**
     * This method overrides the basic toString method from the String class.
     *
     * @return This method returns a special made string for the charPair objects.
     */

    @Override
    public String toString() {
        return "[" + latin + ", " + greek + "]";
    }
}

