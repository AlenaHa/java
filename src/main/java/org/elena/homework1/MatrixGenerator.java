package org.elena.homework1;


/**
 * @author Elena Hardon
 * @date 2/26/17.
 */
public class MatrixGenerator {
    // The single instance of the class.
    private static MatrixGenerator matrixGenerator;


    /**
     * This represents the private constructor od the class.
     */
    private MatrixGenerator() {
    }

    /**
     * This method returns the instance of the class(if this exists) or it creates a new one.
     *
     * @return The class instance.
     */
    static MatrixGenerator getInstance() {
        if (matrixGenerator == null) {
            matrixGenerator = new MatrixGenerator();
        }
        return matrixGenerator;
    }

    /**
     * This method generates all possible matrices (latin or greek squares).
     *
     * @param language   The type of the alphabet that is used.
     * @param matrixSize The size of the matrices that are about to be generated.
     */
    private void generateAllMatrices(Language language, int matrixSize, int maximumAppearances) {
        AlphabetGenerator alphabetGenerator = new AlphabetGenerator(matrixSize, language);


        for (String alphabetCombo : alphabetGenerator.allAlphabetComboStrings) {
            while (maximumAppearances > 0) {
                CharPairMatrix matrix = new CharPairMatrix(matrixSize);
                int k = 1;
                for (int j = 0; j < matrixSize; j++) {
                    CharPair charPair = new CharPair();
                    if (language.equals(Language.latin)) {
                        charPair.setLatin(alphabetCombo.charAt(k));
                    } else {
                        charPair.setGreek(alphabetCombo.charAt(k));
                    }
                    matrix.setElement(0, j, charPair);
                    k++;
                }

                // Completing the next greekMatrixSize -1 rows of the matrix by shifting right by one element the above row.
                for (int i = 1; i < matrixSize; i++) {
                    for (int j = 0; j < matrixSize - 1; j++) {
                        matrix.setElement(i, j, matrix.getElement(i - 1, j + 1));
                    }
                    matrix.setElement(i, matrixSize - 1, matrix.getElement(i - 1, 0));
                }

                matrix.printMatrix(language);
                System.out.println("---------");
                maximumAppearances--;
            }
        }

    }

    /**
     * This method generates a matrix made from the first matrixSize characters from the given alphabet type.
     *
     * @param matrixSize The size of the matrix.
     * @param language   The language of the latin/greek square.
     * @return This method returns a greek/latin square.
     */
    public CharPairMatrix generateMatrix(int matrixSize, Language language) {
        CharPairMatrix matrix = new CharPairMatrix(matrixSize);
        AlphabetGenerator alphabetGenerator = new AlphabetGenerator(matrixSize, language);

        // Completing the first row of the matrix with the specific alphabet letters.
        int k = 0;
        for (int j = 0; j < matrixSize; j++) {
            CharPair charPair = new CharPair();
            if (language.equals(Language.greek)) {
                charPair.setGreek(alphabetGenerator.alphabetArray.get(k));
            } else {
                charPair.setLatin(alphabetGenerator.alphabetArray.get(k));
            }
            matrix.setElement(0, j, charPair);
            k++;
        }

        // Completing the next alphabetMatrixSize -1 rows of the matrix by shifting right by one element the above row.
        for (int i = 1; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize - 1; j++) {
                matrix.setElement(i, j, matrix.getElement(i - 1, j + 1));
            }
            matrix.setElement(i, matrixSize - 1, matrix.getElement(i - 1, 0));
        }
        matrix.printMatrix(language);
        return matrix;
    }

    public static void main(String[] args) {
        MatrixGenerator matrixGenerator = new MatrixGenerator();
        matrixGenerator.generateAllMatrices(Language.latin, 4, 5);
//        matrixGenerator.generateMatrix(3, Language.greek);
    }
}
