package org.elena.homework2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Elena Hardon
 * @date 2/28/17.
 */
public class Teacher {
    // Teacher name.
    private String teacherName;
    // The list containing the teacher's preffered students.
    private List<Student> preferredStudents;

    /**
     * This method represents the constructor of the class that initializes the
     * name of the teacher with the given one as a parameter and initialising the preferred students list.
     * @param teacherName The teacher's name.
     */
    Teacher(String teacherName) {
        this.teacherName = teacherName;
        this.preferredStudents = new ArrayList<>();
    }

    /**
     * This method returns the name of the teacher.
     * @return The teacher's name.
     */
    public String getTeacherName() {
        return teacherName;
    }

    /**
     * This method sets the teacher name.
     * @param teacherName The name that is about to be assigned to the new professor.
     */
    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    /**
     * This method returns the list containing the students that are preferred by the teacher.
     * @return The list of preferred studentts.
     */
    public List<Student> getPreferredStudents() {
        return preferredStudents;
    }

    /**
     * This method sets the list of students using the given one as a parameter.
     * @param preferredStudents The list of preferred students.
     */
    public void setPreferredStudents(List<Student> preferredStudents) {
        this.preferredStudents = preferredStudents;
    }

    /**
     * This method overrides the method toString.
     * @return The object as a string.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Teacher name: ").append(teacherName);
        return sb.toString();
    }

    /**
     * This method overrides the equals method that returns the equality value between the given obj an the current one.
     * @param obj The object to be compared.
     * @return The boolean value returned after the comparation.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Teacher)) {
            return false;
        }

        Teacher teacher = (Teacher) obj;

        return (teacher.teacherName.equals(teacherName));
    }
}
