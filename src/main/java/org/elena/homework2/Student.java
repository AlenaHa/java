package org.elena.homework2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Elena Hardon
 * @date 2/28/17.
 */
public class Student {
    private String studentName;
    private String studentEmail;

    private List<Project> preferedProjects;
    private Project finalProject;


    /**
     * This method represents the constructor of the class that instatiates the student name, student email
     * and instantiates the preferred projects list.
     * @param studentName The name of the student.
     * @param studentEmail The student's email.
     */
    Student(String studentName, String studentEmail) {
        this.studentName = studentName;
        this.studentEmail = studentEmail;
        this.preferedProjects = new ArrayList<>();
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public List<Project> getPreferedProjects() {
        return preferedProjects;
    }

    public void setPreferedProjects(List<Project> preferedProjects) {
        this.preferedProjects = preferedProjects;
    }

    public Project getFinalProject() {
        return finalProject;
    }

    public void setFinalProject(Project finalProject) {
        this.finalProject = finalProject;
    }

    /**
     * This method has changeable number of Project parameters and each project that is a parameter will be added to
     * the student's list of preferred projects.
     * @param projects The projects that are about to be added in the list.
     */
    public void setPreferences(Project... projects) {
        for (Project project : projects) {
            this.preferedProjects.add(project);
        }

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Student name: ").append(studentName).append(", Student e-mail: ").append(studentEmail);
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Student)) {
            return false;
        }

        Student student = (Student) obj;

        return (student.studentEmail.equals(studentEmail) && student.studentName.equals(studentName) && student.finalProject.equals(finalProject));
    }
}
