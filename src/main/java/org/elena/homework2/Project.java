package org.elena.homework2;

/**
 * @author Elena Hardon
 * @date 2/28/17.
 */
public class Project {
    private Teacher coordinator;
    private String projectName;
    private int studentsCapacity;

    /**
     * This method represents the class constructor.
     * @param projectName The name of the new project.
     * @param studentsCapacity The capacity of students for the project.
     */
    Project(String projectName, int studentsCapacity) {
        this.projectName = projectName;
        this.studentsCapacity = studentsCapacity;
    }

    public Teacher getCoordinator() {
        return coordinator;
    }

    public void setCoordinator(Teacher coordinator) {
        this.coordinator = coordinator;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public int getStudentsCapacity() {
        return studentsCapacity;
    }

    public void setStudentsCapacity(int studentsCapacity) {
        this.studentsCapacity = studentsCapacity;
    }


    /**
     * This method adds each Student object from the parameter list into the list of preferred students.
     * @param students The preffered students.
     */
    public void setPreferences(Student... students) {
        for (Student student : students) {
            this.coordinator.getPreferredStudents().add(student);
        }
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Project name : ").append(projectName).append(", Project capacity: ").append(studentsCapacity);
        return sb.toString();

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Project)) {
            return false;
        }

        Project project = (Project) obj;

        return (project.projectName.equals(projectName) && project.studentsCapacity == studentsCapacity && project.coordinator.equals(coordinator));
    }


}
