package org.elena.homework2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Elena Hardon
 * @date 2/28/17.
 */
public class Problem {

    private int numberOfStudents;
    private int numberOfProjects;

    // TODO : Are these needed?
    private List<Student> students;
    private List<Project> projects;

    /**
     * This method is the class constructor that initializes the number of students and projects that will be used in the problem.
     * @param numberOfStudents The number of the students.
     */
    Problem(int numberOfStudents, int numberOfProjects) {
        this.numberOfProjects = numberOfProjects;
        this.projects = new ArrayList<>();
        this.students = new ArrayList<>();
    }

    public Student createStudent(String studentName, String emailAddress) {
        Student newStudent = new Student(studentName, emailAddress);
        this.students.add(newStudent);
        return newStudent;
    }

    public Project createProject(String projectName, int capacity) {
        Project newProject = new Project(projectName, capacity);
        this.projects.add(newProject);
        return newProject;
    }



    public static void main(String[] args) {
        Problem problem = new Problem(4, 3);

        Project p1 = problem.createProject("P1", 4);
        Project p2 = problem.createProject("P2", 4);
        Project p3 = problem.createProject("P3", 4);

        Student s1 = problem.createStudent("S1", "s1@info.uaic.ro");
        Student s2 = problem.createStudent("S2", "s2@info.uaic.ro");
        Student s3 = problem.createStudent("S3", "s3@info.uaic.ro");
        Student s4 = problem.createStudent("S4", "s4@info.uaic.ro");

        Teacher teacher = new Teacher("Acf");

        p1.setCoordinator(teacher);
        s1.setPreferences(p1, p2, p3);
        p1.setPreferences(s3, s1, s2, s4);
        System.out.println(problem);

        System.out.println("Project1 preferred students: " + p1.getCoordinator().getPreferredStudents());
        System.out.println("Student1 preferred projects: " + s1.getPreferedProjects());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Problem{");
        sb.append("numberOfStudents=").append(numberOfStudents);
        sb.append(", numberOfProjects=").append(numberOfProjects);
        sb.append(", projects=").append(projects);
        sb.append(", students=").append(students);
        sb.append('}');
        return sb.toString();
    }
}
