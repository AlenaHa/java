/**
 * @author Elena Hardon
 * @date 3/6/17.
 */
public class Weapon extends AbstractItem implements Item {

    public Weapon(String itemName, int itemWeight, double itemValue) {
        this.setItemName(itemName);
        this.setItemValue(itemValue);
        this.setItemWeight(itemWeight);
    }

}
