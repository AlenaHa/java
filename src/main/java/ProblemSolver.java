import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Elena Hardon
 * @date 3/8/17.
 */
public class ProblemSolver {

    public Knapsack knapsack;
    public List<AbstractItem> finalKnapsackItems;

    ProblemSolver(AbstractItem... items) {
        finalKnapsackItems = new ArrayList<>();
        for (AbstractItem item : items) {
            finalKnapsackItems.add(item);
        }
    }

    public void sortItems() {
        Collections.sort(finalKnapsackItems);
    }
//    @Override
//    public int compare(AbstractItem o1, AbstractItem o2) {
//        if (o1.getProfitFactor() == o2.getProfitFactor()) {
//            return 0;
//        } else return (o2.getProfitFactor() < o1.getProfitFactor()) ? -1 : 1;
//    }

    void knapsackSolver(int totalWeight) {
        knapsack = new Knapsack(totalWeight);
        int weight = 0;
        int totalPrice = 0;

        for (AbstractItem item : finalKnapsackItems) {

            if (weight > totalWeight) {
                break;
            } else if (weight + item.getItemWeight() <= knapsack.getMaximumWeigth()) {
                System.out.println(item);
                weight += item.getItemWeight();
                totalPrice += item.getItemValue();

            } else {
                System.out.println("Partially taking from item: " + item);
                totalPrice += (knapsack.getMaximumWeigth() - weight) * item.getProfitFactor();
                weight = totalWeight;
                break;
            }
        }


        System.out.println(" This knapsack has a total price of : " + totalPrice);
    }


    public void printList() {
        for (AbstractItem item : finalKnapsackItems) {
            System.out.println(item.getItemName() + " value: " + item.getItemValue() + " weight: "
                    + item.getItemWeight() + " profit: " + item.getProfitFactor());
        }
    }

    public static void main(String[] args) {
        AbstractItem book = new Book("Dracula", 1, 2.0);
        AbstractItem pizza = new Food("Pizza", 1, 8.0);
        AbstractItem gun = new Weapon("Revolver", 5, 100.0);
        ProblemSolver problemSolver = new ProblemSolver(pizza, book, gun);
        problemSolver.sortItems();
//        problemSolver.printList();
        problemSolver.knapsackSolver(6);

    }


}
