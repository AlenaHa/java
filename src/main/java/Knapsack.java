import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Elena Hardon
 * @date 3/6/17.
 */
public class Knapsack {
    private int maximumWeigth;
    private ArrayList<AbstractItem> items;
    private int cost;

    Knapsack(int maximumWeigth) {
        this.maximumWeigth = maximumWeigth;
        this.items = new ArrayList<>();
        this.cost = 0;
    }

    public void addItem(AbstractItem item) {
        if ((item.getItemWeight() <= maximumWeigth)) {
            items.add(item);
            cost = cost + item.getItemWeight();
        }
    }

    public ArrayList<AbstractItem> getItemsList() {
        return items;
    }

    public AbstractItem getItemFromList(int index) {
        return this.items.get(index);
    }

    public void deleteItemFromList(int index) {
        items.remove(index);
    }

    public void sortList() {
        Collections.sort(items);
    }

    public int getMaximumWeigth() {
        return maximumWeigth;
    }

    public void updateMaximumWeight(int newMaxWeight) {
        this.maximumWeigth = newMaxWeight;
    }


}

