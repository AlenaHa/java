/**
 * @author Elena Hardon
 * @date 3/6/17.
 */
public class Book extends AbstractItem implements Item {

    public Book(String itemName, int itemWeight, double itemValue) {
        this.setItemName(itemName);
        this.setItemValue(itemValue);
        this.setItemWeight(itemWeight);
    }

}
